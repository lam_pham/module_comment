<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/24/18
 * Time: 10:31 PM
 */
namespace Test\Comment\Model;


class Comment extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_OPENED = 1;
    const STATUS_CLOSED = 0;
    public static $statusesOptions = [
        self::STATUS_OPENED => 'Allow',
        self::STATUS_CLOSED => 'Disable',
    ];
    protected function _construct()
    {
        $this->_init('Test\Comment\Model\ResourceModel\Comment');
    }


    public static function getStatusesOptionArray()
    {
        $result = [];

        foreach (self::$statusesOptions as $value => $label) {
            $result[] = [
                'label' => $label,
                'value' => $value
            ];
        }

        return $result;
    }

    public function getStatusAsLabel()
    {
        return self::$statusesOptions[$this->getStatus()];
    }

}