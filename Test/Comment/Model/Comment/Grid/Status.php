<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/26/18
 * Time: 9:40 PM
 */

namespace Test\Comment\Model\Comment\Grid;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return \Test\Comment\Model\Comment::getStatusesOptionArray();
    }
}
