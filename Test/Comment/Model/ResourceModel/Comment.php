<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/24/18
 * Time: 10:35 PM
 */
namespace Test\Comment\Model\ResourceModel;
class Comment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('test_comment_comment','comment_id');
    }
}