<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/24/18
 * Time: 10:39 PM
 */
namespace Test\Comment\Model\ResourceModel\Comment;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Test\Comment\Model\Comment','Test\Comment\Model\ResourceModel\Comment');
    }
}