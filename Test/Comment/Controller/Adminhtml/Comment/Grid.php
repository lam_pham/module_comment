<?php

namespace Test\Comment\Controller\Adminhtml\Comment;

class Grid extends \Test\Comment\Controller\Adminhtml\Comment
{
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
