<?php

namespace Test\Comment\Controller\Adminhtml\Comment;

class Allow extends \Test\Comment\Controller\Adminhtml\Comment
{
    protected $commentFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Test\Comment\Model\CommentFactory $commentFactory
    )
    {
        $this->commentFactory = $commentFactory;
        parent::__construct($context, $resultPageFactory, $resultForwardFactory);
    }

    public function execute()
    {
        $ticketId = $this->getRequest()->getParam('id');
        $ticket = $this->commentFactory->create()->load($ticketId);

        if ($ticket && $ticket->getId()) {
            try {
                if ($ticket->getStatus() == 0)
                {
                    $ticket->setStatus(\Test\Comment\Model\Comment::STATUS_OPENED);
                }else{
                    $ticket->setStatus(\Test\Comment\Model\Comment::STATUS_CLOSED);
                }

                $ticket->save();
                $this->messageManager->addSuccess(__('Change status successfully closed.'));

            } catch (Exception $e) {
                $this->messageManager->addError(__('Error with change status action.'));
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/index');

        return $resultRedirect;
    }
}
