<?php

namespace Test\Comment\Controller\Adminhtml\Comment;

class Index extends \Test\Comment\Controller\Adminhtml\Comment
{
    public function execute()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('grid');
            return $resultForward;
        }
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Test_Comment::comment_manage');
        $resultPage->getConfig()->getTitle()->prepend(__('Comments'));

        $resultPage->addBreadcrumb(__('Comments'), __('Comments'));
        $resultPage->addBreadcrumb(__('Manage Comments'), __('Manage Comments'));

        return $resultPage;
    }
}
