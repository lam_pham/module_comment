<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/25/18
 * Time: 9:19 AM
 */
namespace Test\Comment\Controller;
abstract class Comment extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }
}