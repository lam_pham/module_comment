<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/25/18
 * Time: 9:13 AM
 */

namespace Test\Comment\Controller\Comment;

class Save extends \Test\Comment\Controller\Comment
{
    protected $inlineTranslation;
    protected $scopeConfig;
    protected $storeManager;
//    protected $formKeyValidator;
    protected $dateTime;
    protected $commentFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Test\Comment\Model\CommentFactory $commentFactory
    )
    {
        $this->scopeConfig = $scopeConfig;
//        $this->formKeyValidator = $formKeyValidator;
        $this->dateTime = $dateTime;
        $this->commentFactory = $commentFactory;
        parent::__construct($context, $customerSession);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

//        if (!$this->formKeyValidator->validate($this->getRequest())) {
//            return $resultRedirect->setRefererUrl();
//        }

        $commentText = $this->getRequest()->getParam('comment','');
        $productId   = $this->getRequest()->getParam('product_id','0');
        $parentId    = $this->getRequest()->getParam('parent_id','0');

        try {
            /* Save comment */
            $comment = $this->commentFactory->create();
            $comment->setCustomerId($this->customerSession->getCustomerId());
            $comment->setProductId($productId);
            $comment->setParentId($parentId);
            $comment->setCommentText($commentText);
            $comment->setCreatedAt($this->dateTime->formatDate(true));
            $comment->setStatus(0);
            $comment->save();


            $this->messageManager->addSuccess(__('Comment successfully created.'));
        } catch (Exception $e) {
            $this->messageManager->addError(__('Error occurred during Comment creation.'));
        }

        return $resultRedirect->setRefererUrl();
    }
}