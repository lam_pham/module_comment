<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/24/18
 * Time: 10:43 PM
 */


namespace Test\Comment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('test_comment_comment'))
            ->addColumn(
                'comment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Comment Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true],
                'Customer Id'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true],
                'Product Id'
            )
            ->addColumn(
                'parent_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true],
                'Parent Id'
            )
            ->addColumn(
                'comment_text',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Comment Text'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false],
                'Created At'
            )
            ->addIndex(
                $installer->getIdxName('test_comment_comment', ['customer_id']),
                ['customer_id']
            )
            ->addIndex(
                $installer->getIdxName('test_comment_comment', ['product_id']),
                ['product_id']
            )
//            ->addForeignKey(
//                $installer->getFkName('test_comment_comment', 'customer_id', 'customer_entity', 'entity_id'),
//                'customer_id',
//                $installer->getTable('customer_entity'),
//                'entity_id',
//                \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
//            )
//            ->addForeignKey(
//                $installer->getFkName('test_comment_comment', 'product_id',
//                    'catalog_product_entity', 'entity_id'),
//                'product_id',
//                $installer->getTable('catalog_product_entity'),
//                'entity_id',
//                \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
//            )
            ->setComment('Test Comment');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
