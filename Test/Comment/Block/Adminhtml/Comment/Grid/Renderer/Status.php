<?php

namespace Test\Comment\Block\Adminhtml\Comment\Grid\Renderer;

class Status extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $commentFactory;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Test\Comment\Model\CommentFactory $commentFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->commentFactory = $commentFactory;
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        $comment = $this->commentFactory->create()->load($row->getId());

        if ($comment && $comment->getId()) {
            return $comment->getStatusAsLabel();
        }

        return '';
    }
}