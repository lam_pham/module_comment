<?php

namespace Test\Comment\Block\Adminhtml;

class Comment extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_comment';
        $this->_blockGroup = 'Test_Comment';
        $this->_headerText = __('Manage Comments');

        parent::_construct();

        $this->removeButton('add');
    }
}
