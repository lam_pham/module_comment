<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/24/18
 * Time: 11:07 PM
 */

namespace Test\Comment\Block\Comment;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Test\Comment\Model\CommentFactory
     */
    protected $commentFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Customer\Model\Session $customerSession,
        \Test\Comment\Model\CommentFactory $commentFactory,
        array $data = []
    )
    {
        $this->dateTime = $dateTime;
        $this->customerSession = $customerSession;
        $this->commentFactory = $commentFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \Test\Comment\Model\ResourceModel\Comment\Collection
     */
    public function getComments($product_id)
    {
        return $this->commentFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter('product_id',array('eq'=>$product_id))
            ->addFieldToFilter('status',array('eq'=>1));
    }
    /**
     * @return \Test\Comment\Model\ResourceModel\Comment\Collection
     */
    public function getCommentByParentId($parent_id)
    {
        return $this->commentFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter('parent_id', $parent_id)
            ->addFieldToFilter('status',array('eq'=>1));
    }
}
